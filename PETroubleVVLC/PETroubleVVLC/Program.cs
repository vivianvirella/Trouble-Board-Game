﻿using System;

namespace PETroubleVVLC
{
    /*Program Name: Practice Exercise 2 VV LC
      Date: 8/24/2020
      Creator: Vivian Virella and Logan Clouse
      Description: Attributes and Methods for Trouble (The Board Game)*/
    class Program
    {
        static void Main(string[] args)
        {
            //attributes
            /* Players (2-4)
             *      Name
             *      4 pieces per player
             *      Color of pieces
             * Board
             *      Dice
             *      Dice popper
             *      Spaces to land on
             *      Starting spot
             *      Home base (4 colors)
             *      Finish area (4 colors)*/
            //methods
            /* Rules
             * Starting/setting up the game
             * Rolling dice
             *      Turn order (Moves to the left from the one that rolls the highest)
             *      Have to leave home base by rolling a 6
             *      Getting to reroll for another turn when you get a 6
             * Moving pieces
             *      Player location
             *      Move pieces clockwise around board
             *      Can't land on your own piece
             *      Send opponent piece to home base when you land on them
             *      Can't move into opponent's finish areas
             * Winning
             *      Need the exact number roll to get into finish area
             *      Need all 4 pieces in finish area to win
             *      Keep playing to find out who wins 2nd and 3rd place*/
        }
    }
}
